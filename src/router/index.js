import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/pages/home/Home'
import Categoria from '@/pages/categoria/Categoria'
import Perfil from '@/pages/perfil/Perfil'
import Post from '@/pages/post/Post'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/categoria/:categoria',
        name: 'Categoria',
        component: Categoria
    },
    {
        path: '/autor/:perfil',
        name: 'Perfil',
        component: Perfil
    },
    {
        path: '/:post',
        name: 'Post',
        component: Post
    }
  ]
})
